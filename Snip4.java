package snipping_tool;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class Snip4 extends JFrame {
	JPanel pnl, pnlWi, pnlHi, pnlCo;
	JWindow wnd;
	JFrame frm;
	Dimension dim;
	static int yPnl, xPnl, wDpnl, hDpnl, wHpnl;

	Snip4() {
		setVisible(true);
		setSize(200, 200);
		dim = getToolkit().getScreenSize();
		setLocation(dim.width / 2 - getWidth() / 2, dim.height / 2
				- getHeight() / 2);
		setResizable(false);
		setLayout(null);
		setAlwaysOnTop(true);
		addWindowListener(new WindowAdapter() {
			public void windowDeactivated(WindowEvent e) {
				setAlwaysOnTop(false);
			}
		});
		addWindowListener(new WindowAdapter() {
			public void windowActivated(WindowEvent e) {
				setAlwaysOnTop(true);
			}
		});
		JButton b = new JButton("Select");
		setLayout(new FlowLayout());
		add(b);
		b.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				wnd.setVisible(true);
				wnd.setAlwaysOnTop(true);
				requestFocus();
				setLocation(-500, 0);
			}
		});
		getContentPane().setBackground(new Color(150, 0, 150));
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		wnd = new Wn();
		wnd.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent e) {
				pnl.setLocation(e.getXOnScreen(), e.getYOnScreen());
				requestFocus();
			}

			public void mouseReleased(MouseEvent e) {
				pnlWi.setBounds(pnl.getWidth() - 10, 0, 10,
						pnl.getHeight() - 10);
				pnlHi.setBounds(0, pnl.getHeight() - 10, pnl.getWidth() - 10,
						10);
				pnlCo.setLocation(pnl.getWidth() - 10, pnl.getHeight() - 10);
			}
		});
		wnd.addMouseMotionListener(new MouseMotionAdapter() {
			public void mouseDragged(MouseEvent e) {
				pnl.setSize(e.getXOnScreen() - pnl.getX(), e.getYOnScreen()
						- pnl.getY());
			}
		});
		addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == 27) {
					wnd.setVisible(false);
					setLocation(dim.width / 2 - getWidth() / 2, dim.height / 2
							- getHeight() / 2);
				}
				if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
					pnl.setSize(pnl.getWidth() + 5, pnl.getHeight());
					pnlWi.setBounds(pnl.getWidth() - 10, 0, 10,
							pnl.getHeight() - 10);
					pnlHi.setBounds(0, pnl.getHeight() - 10,
							pnl.getWidth() - 10, 10);
					pnlCo.setLocation(pnl.getWidth() - 10, pnl.getHeight() - 10);
				}
				if (e.getKeyCode() == KeyEvent.VK_LEFT) {
					pnl.setSize(pnl.getWidth() - 5, pnl.getHeight());
					pnlWi.setBounds(pnl.getWidth() - 10, 0, 10,
							pnl.getHeight() - 10);
					pnlHi.setBounds(0, pnl.getHeight() - 10,
							pnl.getWidth() - 10, 10);
					pnlCo.setLocation(pnl.getWidth() - 10, pnl.getHeight() - 10);
				}
				if (e.getKeyCode() == KeyEvent.VK_DOWN) {
					pnl.setSize(pnl.getWidth(), pnl.getHeight() + 5);
					pnlHi.setBounds(0, pnl.getHeight() - 10,
							pnl.getWidth() - 10, 10);
					pnlWi.setBounds(pnl.getWidth() - 10, 0, 10,
							pnl.getHeight() - 10);
					pnlCo.setLocation(pnl.getWidth() - 10, pnl.getHeight() - 10);
				}
				if (e.getKeyCode() == KeyEvent.VK_UP) {
					pnl.setSize(pnl.getWidth(), pnl.getHeight() - 5);
					pnlHi.setBounds(0, pnl.getHeight() - 10,
							pnl.getWidth() - 10, 10);
					pnlWi.setBounds(pnl.getWidth() - 10, 0, 10,
							pnl.getHeight() - 10);
					pnlCo.setLocation(pnl.getWidth() - 10, pnl.getHeight() - 10);
				}
				if (e.getKeyCode() == e.VK_W) {
					pnl.setLocation(pnl.getX(), pnl.getY() - 5);
				}
				if (e.getKeyCode() == e.VK_S) {
					pnl.setLocation(pnl.getX(), pnl.getY() + 5);
				}
				if (e.getKeyCode() == e.VK_A) {
					pnl.setLocation(pnl.getX() - 5, pnl.getY());
				}
				if (e.getKeyCode() == e.VK_D) {
					pnl.setLocation(pnl.getX() + 5, pnl.getY());
				}
				if (e.getKeyCode() == 32) {
					saving();
				}
			}
		});
	}

	class Wn extends JWindow {
		Wn() {
			setLayout(null);
			setVisible(false);
			setSize(getToolkit().getScreenSize());
			setBackground(new Color(0, 0, 0, 1));
			setCursor(new Cursor(Cursor.CROSSHAIR_CURSOR));
			pnl = new JPanel();
			add(pnl);
			pnl.setBounds(0, 0, 00, 00);
			pnl.setBorder(BorderFactory
					.createLineBorder(new Color(0, 162, 232)));
			pnl.setBackground(new Color(0, 0, 0, 0));
			pnl.setCursor(new Cursor(Cursor.MOVE_CURSOR));
			pnl.addMouseListener(new MouseAdapter() {
				public void mousePressed(MouseEvent e) {
					if (e.getModifiers() == MouseEvent.BUTTON1_MASK) {
						xPnl = e.getXOnScreen();
						yPnl = e.getYOnScreen();
					}
					if (e.getModifiers() == MouseEvent.BUTTON3_MASK) {
						wnd.setAlwaysOnTop(false);
						saving();
						wnd.setAlwaysOnTop(true);
					}
				}
			});
			pnl.addMouseMotionListener(new MouseAdapter() {
				public void mouseDragged(MouseEvent e) {
					pnl.setLocation(pnl.getX() + e.getXOnScreen() - xPnl,
							pnl.getY() + e.getYOnScreen() - yPnl);
					// pnlWi.setBounds(pnl.getWidth()-10, 0, 10,
					// pnl.getHeight()-10);
					xPnl = e.getXOnScreen();
					yPnl = e.getYOnScreen();
				}
			});
			pnl.addMouseWheelListener(new MouseWheelListener() {
				public void mouseWheelMoved(MouseWheelEvent e) {
					pnl.setSize(pnl.getWidth() - e.getUnitsToScroll(),
							pnl.getHeight() - e.getUnitsToScroll());
					pnlWi.setBounds(pnl.getWidth() - 10, 0, 10,
							pnl.getHeight() - 10);
					pnlHi.setBounds(0, pnl.getHeight() - 10,
							pnl.getWidth() - 10, 10);
					pnlCo.setLocation(pnl.getWidth() - 10, pnl.getHeight() - 10);
				}
			});
			pnl.setLayout(null);
			pnlWi = new JPanel();
			pnlHi = new JPanel();
			pnlCo = new JPanel();
			pnlCo.setSize(10, 10);
			pnl.add(pnlWi);
			pnl.add(pnlHi);
			pnl.add(pnlCo);
			pnlWi.setCursor(new Cursor(Cursor.W_RESIZE_CURSOR));
			pnlHi.setCursor(new Cursor(Cursor.N_RESIZE_CURSOR));
			pnlCo.setCursor(new Cursor(Cursor.SE_RESIZE_CURSOR));
			pnlWi.setBackground(new Color(0, 0, 0, 0));
			pnlHi.setBackground(new Color(0, 0, 0, 0));
			pnlCo.setBackground(new Color(0, 0, 0, 0));
			pnlWi.addMouseListener(new MouseAdapter() {
				public void mousePressed(MouseEvent e) {
					wDpnl = e.getXOnScreen();
				}
			});
			pnlHi.addMouseListener(new MouseAdapter() {
				public void mousePressed(MouseEvent e) {
					hDpnl = e.getYOnScreen();
				}
			});
			pnlCo.addMouseListener(new MouseAdapter() {
				public void mousePressed(MouseEvent e) {
					wDpnl = e.getXOnScreen();
					hDpnl = e.getYOnScreen();
				}
			});
			pnlWi.addMouseMotionListener(new MouseMotionAdapter() {
				public void mouseDragged(MouseEvent e) {
					pnl.setSize(pnl.getWidth() - wDpnl + e.getXOnScreen(),
							pnl.getHeight());
					pnlWi.setBounds(pnl.getWidth() - 10, 0, 10,
							pnl.getHeight() - 10);
					pnlHi.setBounds(0, pnl.getHeight() - 10,
							pnl.getWidth() - 10, 10);
					pnlCo.setLocation(pnl.getWidth() - 10, pnl.getHeight() - 10);
					wDpnl = e.getXOnScreen();
				}
			});
			pnlHi.addMouseMotionListener(new MouseMotionAdapter() {
				public void mouseDragged(MouseEvent e) {
					pnl.setSize(pnl.getWidth(),
							pnl.getHeight() + e.getYOnScreen() - hDpnl);
					pnlHi.setBounds(0, pnl.getHeight() - 10,
							pnl.getWidth() - 10, 10);
					pnlWi.setBounds(pnl.getWidth() - 10, 0, 10,
							pnl.getHeight() - 10);
					pnlCo.setLocation(pnl.getWidth() - 10, pnl.getHeight() - 10);
					hDpnl = e.getYOnScreen();
				}
			});
			pnlCo.addMouseMotionListener(new MouseMotionAdapter() {
				public void mouseDragged(MouseEvent e) {
					pnl.setSize(pnl.getWidth() - wDpnl + e.getXOnScreen(),
							pnl.getHeight() + e.getYOnScreen() - hDpnl);
					pnlWi.setBounds(pnl.getWidth() - 10, 0, 10,
							pnl.getHeight() - 10);
					pnlHi.setBounds(0, pnl.getHeight() - 10,
							pnl.getWidth() - 10, 10);
					pnlCo.setLocation(pnl.getWidth() - 10, pnl.getHeight() - 10);
					wDpnl = e.getXOnScreen();
					hDpnl = e.getYOnScreen();
				}
			});
		}
	}

	public void saving() {
		wnd.setAlwaysOnTop(false);
		Rectangle hui = new Rectangle(pnl.getBounds());
		hui.x++;
		hui.y++;
		hui.width -= 2;
		hui.height -= 2;
		try {
			BufferedImage img = new Robot().createScreenCapture(hui);
			FileDialog fd = new FileDialog(new JFrame(), "SnippetTool",
					FileDialog.SAVE);
			fd.setDirectory("c:\\Video\\Porn");
			fd.setFile("*.jpg");
			fd.setVisible(true);
			try {
				ImageIO.write(img, "jpg",
						new File(fd.getDirectory() + fd.getFile()));
			} catch (IOException ex) {
			}
		} catch (AWTException ex) {
		}
		wnd.setVisible(false);
		setLocation(dim.width / 2 - getWidth() / 2, dim.height / 2
				- getHeight() / 2);
	}

	public static void main(String[] args) {
		new Snip4();
	}
}
